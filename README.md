# Running App 

 
## Features:
* A program to calculate the distance traveled during the run  
* the calories burned and the average speed 
* Display data in more than one way using a graph 

## Tools :       
* Kotlin
* MVVM
* Room database
* viewModel
* LiveData
* Coroutines
* Navigation component 
* Hilt
* View Binding
* RecyclerView 
* Google Maps Location Services
* Easy Permissions [documentation](https://github.com/googlesamples/easypermissions)
* MPAndroidChart [documentation](https://github.com/PhilJay/MPAndroidChart)

### for download apk: [Download](https://drive.google.com/file/d/1oy9aDTr3qCbD7kA5yAWPiE6i6uNyDO45/view?usp=sharing)

## Screens : 
![1](https://user-images.githubusercontent.com/53372814/145170914-74e207f9-3562-4fd2-a573-4216a5834c98.png)![2](https://user-images.githubusercontent.com/53372814/145170958-c89ea2de-21f3-4cfe-91ad-77ffc5f32e1f.png)![3](https://user-images.githubusercontent.com/53372814/145170955-c9620bc8-7443-4c80-8259-b85f6674d642.png)![4](https://user-images.githubusercontent.com/53372814/145170947-1a57fd93-0d21-4101-854a-739299aa4f94.png)![5](https://user-images.githubusercontent.com/53372814/145170950-635257ee-efb7-4627-ad4e-40801fc94cd6.png)


```python
NOTE : to get started put your Google_Maps_API_Key in Strings file before you run the app.
